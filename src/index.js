function nonEmptyElementsFilter(e) {
  return !!e;
}

function elementToClassName(element) {
  if (!element) return null;

  var element_type = typeof element;

  if (element_type === 'string') {
    if (element.length <= 0) return null;
    return element;
  }
  if (element_type === 'number') {
    return element_type;
  }

  if (Array.isArray(element)) {
    return element.map(elementToClassName)
      .filter(nonEmptyElementsFilter)
      .join(' ');
  }

  if (element_type === 'object') {
    return Object.keys(element)
      .map(function(key) { return !!element[key] ? key : null; })
      .filter(nonEmptyElementsFilter)
      .join(' ');
  }

  console.warn('classNames: invalid element of type', element_type);

  return null;
}

function classNames() {
  if (arguments.length <= 0) return '';
  var args = Array(arguments.length);
  for (var i = 0; i < arguments.length; i++) args[i] = arguments[i];

  return args.map(elementToClassName).filter(nonEmptyElementsFilter).join(' ');
}

module.exports = classNames;
