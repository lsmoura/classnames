classnames
==========

## Installing

```sh
yarn add @lsmoura/classnames
```

```js
import classNames from '@lsmoura/classnames';
console.log(classNames('monkey', 'patch')); // will output 'monkey patch'
```

## Usage

The function can take any number or arguments with types 'string', 'number',
'arrays' and 'objects'. Falsy elements will be ignored. Objects with keys points
to truthy values will be included on the result.

You can pass any kind of nested arrays into the function.

```js
classNames('monkey', 'patch'); // 'monkey patch'
classNames(['monkey', 'patch']); // 'monkey patch'
classNames('monkey', ['patch', 'boo']); // 'monkey patch boo'
classNames('monkey', null, ['patch', 0, 'boo']); // 'monkey patch boo'
classNames('monkey', ['patch'], { boo: true, bar: false }); // 'monkey patch boo'
```
