var assert = require('assert');

var classnames = require('../src');

describe('classnames', () => {
  it('should return an empty string when called with no parameters', () => {
    assert.equal(classnames(), '');
  });
  it('should return an empty string when called with falsy arguments only', () => {
    assert.equal(classnames(null, 0, undefined, false), '');
  });
  it('should return an empty string when called with an array with falsy arguments only', () => {
    assert.equal(classnames([null, 0, undefined, false]), '');
  });
  it('should return the passed value when called with a single string', () => {
    assert.equal(classnames('foo'), 'foo');
  });
  it('should return the concatenated value when called with a multiple strings', () => {
    assert.equal(classnames('foo', 'bar'), 'foo bar');
  });
  it('should return the concatenated value when called mixing strings and arrays', () => {
    assert.equal(classnames('foo', ['bar', 'loop']), 'foo bar loop');
  });
  it('should return truthy keys from object (I)', () => {
    assert.equal(classnames({ foo: true, bar: false }), 'foo');
  });
  it('should return truthy keys from object (II)', () => {
    assert.equal(classnames({ foo: true, bar: 1, loop: 0 }), 'foo bar');
  });
  it('should properly mix elements', () => {
    assert.equal(classnames({ foo: true, loop: 0 }, 'bar', ['monkey', null, 'patch'], undefined, 'stool'), 'foo bar monkey patch stool');
  });
  it('should work with nested arrays', () => {
    assert.equal(classnames(['foo', ['bar', { loop: 1 }, ['monkey', { boo: 0, patch: 1 }]]]), 'foo bar loop monkey patch');
  });
});
